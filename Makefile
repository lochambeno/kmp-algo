# tool macros
CC = gcc
CFLAGS := -Wall -Wextra
TARGET = myExe
SOURCES = KMP.c main.c string.c
HEADERS = KMP.h string.h

$(TARGET): $(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) -o $(TARGET) $(SOURCES) -g

clean:
	rm *.o
	rm $(TARGET)