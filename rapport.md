# Rapport AAC TP2
## Programmes Louis Chambenoit, Rédaction Baptiste Vallet
## 08 / 03 /2023
# Algorithme de Knuth-Morris-Pratt

## Définition
L'algorithme de Knuth-Morris-Pratt, du nom de ses inventeurs, ou KMP est un algorithme pour trouver les occurences d'une chaîne de caractère dans un texte. Son efficacité vient du fait qu'il n'a pas besoin de revenir en arrière dans le texte si la sous partie du texte ne contient pas la chaîne souhaitée. 

## Principe
Le programme fonctionne en deux phases. Une première phase pour analyser la chaîne de caractère. Dans cette analyse, on vérifie s'il y a des motifs récurrents pour savoir combien de caractères sont déjà corrects au cas où il faudrait recommencer la recherche du mot. On retourne un tableau d'indices de retour, qui indique pour chaque caractère de la chaîne, jusqu'où revenir en arrière, autrement dit, le nombre de caractères déjà valides si l'on doit recommencer les comparaisons.

Par exemple : On peut déduire que dans le texte "AABAABAABAC", au lieu de reparcourir le mot "AABAABAC" depuis le début en position 3 du texte lorsqu'on remarque qu'il n'y a pas de C en position 7. On sait déjà que le motif du début "AABA" est correct sans recommencer les comparaison depuis la position  3. Ainsi, on recommence les comparaison à partir du 5e caractère du mot à la 7e position dans le texte.

La seconde phase est le parcours du texte pour vérifier les occurrences d'une chaîne de caractère. Grâce au tableau construit lors de la première phase, On peut ne faire qu'un parcours de la liste sans faire aucun retour en arrière. 

## Structure de Données
### Le type *kmp_result_t*
C'est le type renvoyé par la recherche, il contient le nombre d'occurrences trouvées et la liste des indices du premier caractère des occurences ainsi que la taille du texte parcouru.
### Le type *string_t*
C'est une chaîne de caractère composée d'un tableau dynamique de caractères, le nombre de caractères de la chaîne et la taille du tableau dynamique.

## Algorithmes
### *kmp_table()*
La première phase de l'algortihme. On parcourt la chaîne de caractères, si le caractère est identique au précédent, son indice de retour pointera sur celui de son précédent, sinon il cherche en passant d'indice de retour en indice de retour jusqu'à tomber sur un caractère identique. Sinon, cela vaudra -1, indiquant que si il n'y a pas correspondances pour ce caractère, la comparaison avec le mot reprendra de zéro.  
### *kmp_algorithm()*
La seconde phase de l'algorithme. Parcours du texte et recherche d'une chaîne de caractères avec un tableau d'indices de retours initialisé. La fonction renvoie une variable *kmp_result_t* remplie. Si des caractères en commun sont trouvés, on avance dans le parcours de la chaîne de caractères sinon, on suit le tableau d'indices pour savoir comment continuer. Si une occurence est trouvée, on l'ajoute à la liste des occurences de la valeur de retour. 