#include "KMP.h"

/**************************************************************
 * 
 * L'objectif de ce tableau est de chercher les paterns dans 
 * le mot à trouver. Pour chaque caractére dans ce mot, on va
 * associer un entier qui va rediriger sur un patern du mot
 * dont il peut faire parti.
 * 
 * Par exemple si on cherche le mot ABACABAD, on sait que si
 * l'on trouve ABACABA mais pas le D, on peut reprendre le 
 * mot depuis ABA. 
 * 
***************************************************************/

int * kmp_table(string_t mot){
    int* T = malloc(sizeof(int)*(mot.size+1));

    //pos permet de savoir où on en est dans la chaine
    int pos = 1;

    //cnd pointe sur le premier caractère de la chaine
    int cnd = 0;

    //si on a -1 cela veut dire qu'on doit recommencer depuis le début de la chaine, si le premier caractére est faux, on reprend depuis le début
    T[0] = -1;
    printf("%d ", T[0]);

    while(pos < mot.size){

        //Si les deux caractéres correspondent, la case correspondant au caractére actuel doit pointer sur le caractére
        //précédent qui lui correspond (on a trouvé un pattern) 
        if(mot.str[pos] == mot.str[cnd]){
            T[pos] = T[cnd];
        }
        //cnd pointe actuellement sur le caractere suivant le dernier pattern trouvé, on doit donc revenir à ce pattern
        //si la suite ne correspond pas
        else{ 
            T[pos] = cnd;
            //on cherche un autre pattern valide au cas où le pattern précédent ne fonctionne pas,
            //s'il y en a pas, on finira pas tomber sur -1 en suivant les cases
            while(cnd >= 0 && mot.str[pos] != mot.str[cnd]){
                cnd = T[cnd];
            }
        }
        
        printf("%d ", T[pos]);

        //on doit vérifier si le caractere suivant est OK pour voir si on a un pattern plus long.
        pos++;
        cnd++;
    }

    T[pos] = cnd;

    printf("\n");
    return T;
}

kmp_result_t kmp_algorithm(string_t phrase, string_t mot_a_trouver, int * T){
    int j = 0;
    int k = 0;

    kmp_result_t result;

    result.positions = malloc(sizeof(int)*phrase.size);
    result.array_size = phrase.size;
    result.number_of_positions = 0;

    while(j < phrase.size){
        printf("Caractere n°%d : k = %d\n", j, k);

        if(mot_a_trouver.str[k] == phrase.str[j]){
            printf("--> Lettre commune trouvée : %c  %d/%d\n", mot_a_trouver.str[k], k+1, mot_a_trouver.size);

            j++;
            k++;
            if(k == mot_a_trouver.size){
                printf("----> Mot trouvé !\n");
                result.positions[result.number_of_positions] = j - k;
                result.number_of_positions++;
                k = T[k];
            }
        }
        else{
            k = T[k];
            if(k<0){
                j++;
                k++;
            }
        }
    }

    return result;
}
