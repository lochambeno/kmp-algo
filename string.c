#include "string.h"

string_t init_string(char string[], int size)
{
    string_t result;
    
    result.str = malloc(size*sizeof(char));

    for(int i = 0; i < size; i++){
        result.str[i] = string[i];
    }

    result.size = size;
    result.table_size = size;
    
    return result;
}

void delete_string(string_t to_delete){
    if(to_delete.str != NULL){
        free(to_delete.str);
        to_delete.str = NULL;
    }
    to_delete.size = 0;
    to_delete.table_size = 0;
}