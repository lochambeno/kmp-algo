#ifndef AAC_STRING_H
#define AAC_STRING_H

#include "stdio.h"
#include "stdlib.h"

typedef struct string{
    char * str;
    int size;
    int table_size;
} string_t;

string_t init_string(char string[], int size);
void delete_string(string_t to_delete);

#endif