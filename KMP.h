#ifndef AAC_KMP_H
#define AAC_KMP_H

#include "string.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct kmp_result{
    int * positions;
    int number_of_positions;
    int array_size;
}kmp_result_t;

int * kmp_table(string_t mot);
kmp_result_t kmp_algorithm(string_t phrase, string_t mot_a_trouver, int * T);

#endif