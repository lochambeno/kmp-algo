#include "KMP.h"
#include "string.h"

int main(){
    //tests 

    string_t phrase = init_string("ababdababadcbadbababac", 24);
    string_t mot    = init_string("ababadc", 7);
    
    printf("********************************************************\n\n");
    printf("Test de l'algo avec la phrase : '%s' : \n\n", phrase.str);
    printf("********************************************************\n");

    printf("Trace de l'éxecution :\n\n");

    printf("Valeurs de la table T :\n");
    int * T = kmp_table(mot);

    printf("********************************************************\n");
    kmp_result_t positions = kmp_algorithm(phrase, mot, T);

    printf("********************************************************\n");
    printf("Liste des positions du mot '%s' :\n", mot.str);
    for(int i = 0; i < positions.number_of_positions; i++){
        printf("%d\n", positions.positions[i]);
    }
    
    printf("********************************************************\n");

    delete_string(phrase);
    delete_string(mot);

    return 0;
}